import React from 'react';
import { mount } from 'enzyme';
import { ApolloProvider } from 'react-apollo';

import List from './index';

import client from '../../../apollo';

const wrapper = mount(
  <ApolloProvider client={client}>
    <List />
  </ApolloProvider>
);

const instance = wrapper.find(List).instance()

const mockedReceipts = [
  {
    date: "2018-10-01",
    description: "Test receipt",
    imageUrl: "https://example.com",
    itemName: "item name",
    price: 1000,
    uuid: "71568b37-c6c7-4e6b-8020-ffe1e8de8837"
  },
  {
    date: "2018-10-01",
    description: "Test receipt2",
    imageUrl: "https://example.com",
    itemName: "item name",
    price: 2000,
    uuid: "71568b37-c6c7-4e6b-8020-ffe1e8de8838"
  },
  {
    date: "2018-10-02",
    description: "Test receipt3",
    imageUrl: "https://example.com",
    itemName: "item name",
    price: 3000,
    uuid: "71568b37-c6c7-4e6b-8020-ffe1e8de8839"
  }
]

it('Test setList method', () => {
  expect(instance.state.list).toHaveLength(0);
  expect(instance.state.displayList).toHaveLength(0);

  instance.setList(mockedReceipts);

  expect(instance.state.list).toHaveLength(3);
  expect(instance.state.displayList).toHaveLength(3);

  // Broken Data

  instance.setList(undefined);
  expect(instance.state.list).toHaveLength(3);
  expect(instance.state.displayList).toHaveLength(3);

  instance.setList(null);
  expect(instance.state.list).toHaveLength(3);
  expect(instance.state.displayList).toHaveLength(3);

  instance.setList({});
  expect(instance.state.list).toHaveLength(3);
  expect(instance.state.displayList).toHaveLength(3);

  instance.setList('123456');
  expect(instance.state.list).toHaveLength(3);
  expect(instance.state.displayList).toHaveLength(3);
});

it('Test handleFilter method', () => {
  instance.handleFilter({ target: { value: '2018-10-01' } });

  expect(instance.state.displayList).toHaveLength(2);

  instance.handleFilter({ target: { value: 'lorem ipsum' } });
  expect(instance.state.displayList).toHaveLength(0);

  instance.handleFilter({ target: { value: '' } });
  expect(instance.state.list).toHaveLength(3);
});
