import React from 'react';
import { shallow, mount } from 'enzyme';
import { ApolloProvider } from 'react-apollo';
import { spy } from 'sinon';

import Item from './index';

import client from '../../../../apollo';
import { REMOVE_RECEIPT } from '../../../../apollo/mutations';


it('Test useDefaultImage method', () => {
  const wrapper = shallow(<Item />);

  const instance = wrapper.instance();

  let e = { target: { src: '' } };

  instance.useDefaultImage(e);

  expect(e.target.src).toEqual('https://image.flaticon.com/icons/svg/172/172167.svg');
});

it('Test mounted Item Component', () => {
  spy(client, 'mutate');

  const wrapper = mount(
    <ApolloProvider client={client}>
      <Item
        uuid="abc-100"
        imageUrl="https://image.flaticon.com/icons/svg/172/172167.svg"
        itemName="Name"
        date="2018-11-11"
        price={2000}
        description="Lorem Ipsum"
      />
    </ApolloProvider>
  );

  wrapper.find('button.red-element').simulate('click');
  expect(client.mutate.calledOnce).toEqual(true);
  expect(client.mutate.getCall(0).args[0].variables).toEqual({
    uuid: 'abc-100',
  });

  expect(client.mutate.getCall(0).args[0].mutation).toEqual(
    REMOVE_RECEIPT,
  );
});