import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Mutation } from 'react-apollo';

import { REMOVE_RECEIPT } from '../../../../apollo/mutations';
import { GET_USER_RECEIPTS } from '../../../../apollo/queries';

import './styles.scss';


class Item extends PureComponent {
  useDefaultImage(e) {
    e.target.src = "https://image.flaticon.com/icons/svg/172/172167.svg";
  }

  render() {
    return (
      <li className="item">
        <div className="item-wrapper">
          <img src={this.props.imageUrl} alt="receipt" onError={this.useDefaultImage}/>
          <div className="info">
            <h2>{this.props.itemName}</h2>
            <span>Date: {this.props.date}</span>
            <span>Price: {this.props.price}</span>
          </div>
        </div>
        <p>Description: {this.props.description}</p>
        <Mutation
          mutation={REMOVE_RECEIPT}
          refetchQueries={[{query: GET_USER_RECEIPTS}]}
        >
          {(removeReceipt, { error }) => (
            <div className="settings-container">
              <button
                className="red-element"
                onClick={() => removeReceipt({ variables: { uuid: this.props.uuid } })}
              >
                Remove Receipt
              </button>
              {
                (error && error.message.includes("Not found")) && 
                <span className="error-msg">Something went wrong. Receipt can't be removed.</span>
              }
            </div>
          )}
        </Mutation>
      </li>
    );
  }
}

Item.propTypes = {
  uuid: PropTypes.string,
  imageUrl: PropTypes.string,
  itemName: PropTypes.string,
  date: PropTypes.string,
  price: PropTypes.number,
  description: PropTypes.string,
};

export default Item;
