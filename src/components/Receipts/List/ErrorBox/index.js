import React, { PureComponent, Fragment } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import './styles.scss';


class ErrorBox extends PureComponent {
  render() {
    return (
      <div className="error-box">
        { 
          this.props.message.includes("Not authorized") ?
          <Fragment>
            <span>You are not authorized, please</span>
            <Link className="pink-element" to="/sign-in">Sign In</Link>
            <span>or</span>
            <Link className="purple-element" to="/sign-up">Sign Up</Link>
            <span>or</span>
            <Link to="/confirmation">Confirm an Account</Link>
          </Fragment> :
          <Fragment>
            <div className="error-box">
              <span>{this.props.message}</span>
            </div>
          </Fragment>
        }
      </div>
    );
  }
}

ErrorBox.propTypes = {
  message: PropTypes.string,
};

export default ErrorBox;
