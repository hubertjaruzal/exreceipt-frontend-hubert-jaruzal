import React, { Component } from 'react';
import { Query } from 'react-apollo';
import _isEqual from 'lodash/isEqual';

import { GET_USER_RECEIPTS } from '../../../apollo/queries';

import Item from './Item';
import ErrorBox from './ErrorBox';
import Header from './Header';

import './styles.scss';

class Receipts extends Component {
  constructor(props) {
    super(props);

    this.state = {
      list: [],
      displayList: [],
      searchQuery: ""
    }

    this.setList = this.setList.bind(this);
    this.handleFilter = this.handleFilter.bind(this);
  }

  setList(data) {
    if (_isEqual(data, this.state.list) || !Array.isArray(data)) return;
    this.setState({
      list: data,
      displayList: !!this.state.searchQuery.length ? data.filter(item => item.date.startsWith(this.state.searchQuery)) : data
    });
  }

  handleFilter(e) {
    if (!e.target.value.length) return this.setState({
      displayList: this.state.list,
      searchQuery: e.target.value
    });
    this.setState({
      displayList: this.state.list.filter(item => item.date.startsWith(e.target.value)),
      searchQuery: e.target.value
    });
  }

  render() {
    return (
      <Query 
        query={GET_USER_RECEIPTS}
        onCompleted={(data) => this.setList(data.listCurrentUserReceipts)}
        fetchPolicy="network-only"
      >
        {({ loading, error }) => {
          if (loading) return "Loading...";
          if (error) return <ErrorBox message={error.message} />
    
          return (
            <div className="receipts">
              <Header
                handleFilter={this.handleFilter}
                searchQuery={this.state.searchQuery}
              />
              <ul>
                {
                  this.state.displayList.map(receipt => (
                    <Item
                      key={receipt.uuid}
                      uuid={receipt.uuid}
                      imageUrl={receipt.imageUrl}
                      itemName={receipt.itemName}
                      date={receipt.date}
                      price={receipt.price}
                      description={receipt.description}
                    />
                  ))
                }
              </ul>
            </div>
          );
        }}
      </Query>
    );
  }
}

export default Receipts;
