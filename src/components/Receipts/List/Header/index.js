import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';


class Header extends Component {
  render() {
    return (
      <Fragment>
        <Link className="purple-element" to="/receipt/add">New Receipt</Link>
        <label htmlFor="dateFilter">Filter by Date:</label>
        <input
          id="dateFilter"
          onChange={this.props.handleFilter}
          placeholder="YYYY-MM-DD"
          value={this.props.searchQuery}
        />
      </Fragment>
    );
  }
}

Header.propTypes = {
  handleFilter: PropTypes.func,
  searchQuery: PropTypes.string,
};

export default Header;
