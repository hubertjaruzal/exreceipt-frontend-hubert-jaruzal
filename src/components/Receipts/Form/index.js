import React, { Component } from 'react';
import { Mutation } from "react-apollo";

import { CREATE_RECEIPT } from '../../../apollo/mutations';

import './styles.scss';


class ReceiptsForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      date: this.getDefaultDate(),
      description: "",
      imageUrl: "https://image.flaticon.com/icons/svg/172/172167.svg",
      itemName: "",
      price: ""
    }
  }
  
  getDefaultDate() {
    const date = new Date();
    return `${date.getFullYear().toString()}-${("0" + (date.getMonth() + 1)).slice(-2).toString()}-${("0" + (date.getDate())).slice(-2).toString()}`
  }

  handleInputChange(key, value) {
    this.setState({[key]: value})
  }

  render() {
    return (
      <Mutation 
        mutation={CREATE_RECEIPT}
        update={() => this.props.history.push("/")}
      >
        {(createReceipt, { error }) => (
          <div className="receipt-container">
            <h2>Add new receipt</h2>
            <form
              className="receipt-form"
              onSubmit={e => {
                e.preventDefault();
                createReceipt({ 
                  variables: { 
                    date: this.state.date,
                    description: this.state.description,
                    imageUrl: this.state.imageUrl,
                    itemName: this.state.itemName,
                    price: Number(this.state.price)
                  } 
                });
              }}
            >
              <input
                placeholder="Date: YYYY-MM-DD"
                value={this.state.date}
                onChange={(e) => this.handleInputChange("date", e.target.value)}
              ></input>
              <input
                placeholder="Description"
                value={this.state.description}
                onChange={(e) => this.handleInputChange("description", e.target.value)}
              ></input>
              <input
                placeholder="Image URL"
                value={this.state.imageUrl}
                onChange={(e) => this.handleInputChange("imageUrl", e.target.value)}
              ></input>
              <input
                placeholder="Item name"
                value={this.state.itemName}
                onChange={(e) => this.handleInputChange("itemName", e.target.value)}
              ></input>
              <input
                placeholder="Price"
                value={this.state.price}
                onChange={(e) => this.handleInputChange("price", e.target.value)}
              ></input>
              {
                error && 
                <span className="error-msg">{error.message.replace('GraphQL error: ','')}</span>
              }
              <button type="submit" className="purple-element">Submit</button>
            </form>
          </div>
        )}
      </Mutation>
    );
  }
}

export default ReceiptsForm;
