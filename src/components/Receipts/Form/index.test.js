import React from 'react';
import { shallow, mount } from 'enzyme';
import { ApolloProvider } from 'react-apollo';
import { spy } from 'sinon';

import ReceiptsForm from './index';

import client from '../../../apollo';
import { CREATE_RECEIPT } from '../../../apollo/mutations';


const DATE_TO_USE = new Date('2016', '0', '20');
const _Date = Date;
global.Date = jest.fn(() => DATE_TO_USE);
global.Date.UTC = _Date.UTC;
global.Date.parse = _Date.parse;
global.Date.now = _Date.now;

it('Test getDefaultDate method', () => {
  const wrapper = shallow(<ReceiptsForm />);

  const instance = wrapper.instance();

  expect(instance.getDefaultDate()).toEqual('2016-01-20');
});

it('Test mounted Item Component', () => {
  spy(client, 'mutate');

  const wrapper = mount(
    <ApolloProvider client={client}>
      <ReceiptsForm
        uuid="abc-100"
        imageUrl="https://image.flaticon.com/icons/svg/172/172167.svg"
        itemName="Name"
        date="2018-11-11"
        price={2000}
        description="Lorem Ipsum"
      />
    </ApolloProvider>
  );

  const instance = wrapper.find(ReceiptsForm).instance();

  expect(wrapper.find('input')).toHaveLength(5);

  wrapper.find('input').first().simulate('change', { target: { value: '2018-11-11' } });
  expect(instance.state.date).toEqual('2018-11-11');

  wrapper.find('input').at(1).simulate('change', { target: { value: 'Lorem Ipsum' } });
  expect(instance.state.description).toEqual('Lorem Ipsum');

  wrapper.find('input').at(2).simulate('change', { target: { value: 'https://image.flaticon.com/icons/svg/172/172167.svg' } });
  expect(instance.state.imageUrl).toEqual('https://image.flaticon.com/icons/svg/172/172167.svg');

  wrapper.find('input').at(3).simulate('change', { target: { value: 'Receipt Name' } });
  expect(instance.state.itemName).toEqual('Receipt Name');

  wrapper.find('input').at(4).simulate('change', { target: { value: '2000' } });
  expect(instance.state.price).toEqual('2000');

  wrapper.find('button.purple-element').simulate('submit');
  expect(client.mutate.calledOnce).toEqual(true);

  expect(client.mutate.getCall(0).args[0].variables).toEqual({
    date: "2018-11-11",
    description: "Lorem Ipsum",
    imageUrl: "https://image.flaticon.com/icons/svg/172/172167.svg",
    itemName: "Receipt Name",
    price: 2000
  });

  expect(client.mutate.getCall(0).args[0].mutation).toEqual(
    CREATE_RECEIPT,
  );
});