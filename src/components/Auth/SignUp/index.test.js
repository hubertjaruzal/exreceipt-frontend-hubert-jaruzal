import React from 'react';
import { shallow, mount } from 'enzyme';
import { ApolloProvider } from 'react-apollo';
import { BrowserRouter as Router } from 'react-router-dom';
import { spy } from 'sinon';

import SignUp from './index';

import client from '../../../apollo';

import { CREATE_USER } from '../../../apollo/mutations';


it('Test handleInputChange method', () => {
  const wrapper = shallow(<SignUp />);

  const instance = wrapper.instance();

  instance.handleInputChange('email', 'user@example.com');

  expect(instance.state.email).toEqual('user@example.com');
});

it('Test mounted SignUp Component', () => {
  spy(client, 'mutate');

  const wrapper = mount(
    <ApolloProvider client={client}>
      <Router>
        <SignUp />
      </Router>
    </ApolloProvider>
  );

  const instance = wrapper.find(SignUp).instance();

  expect(wrapper.find('input')).toHaveLength(4);

  wrapper.find('input').first().simulate('change', { target: { value: 'user100@example.com' } });
  expect(instance.state.email).toEqual('user100@example.com');

  wrapper.find('input').at(1).simulate('change', { target: { value: 'Hill' } });
  expect(instance.state.familyName).toEqual('Hill');

  wrapper.find('input').at(2).simulate('change', { target: { value: 'John' } });
  expect(instance.state.name).toEqual('John');

  wrapper.find('input').at(3).simulate('change', { target: { value: 'password' } });
  expect(instance.state.password).toEqual('password');

  wrapper.find('button.purple-element').simulate('submit');
  expect(client.mutate.calledOnce).toEqual(true);

  expect(client.mutate.getCall(0).args[0].variables).toEqual({
    email: 'user100@example.com',
    familyName: 'Hill',
    name: 'John',
    password: 'password'
  });

  expect(client.mutate.getCall(0).args[0].mutation).toEqual(
    CREATE_USER,
  );
});