import React, { PureComponent } from 'react';
import { Mutation } from 'react-apollo';
import { Link } from 'react-router-dom';

import { CREATE_USER } from '../../../apollo/mutations';

import '../styles.scss';


class SignUp extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      email: "",
      familyName: "",
      name: "",
      password: "",
    }
  }

  handleInputChange(key, value) {
    this.setState({[key]: value})
  }

  render() {
    return (
      <Mutation
        mutation={CREATE_USER}
        update={() => this.props.history.push("/confirmation")}
      >
        {(createUser, { data, error }) => (
          <div className="auth-container">
            <h2>Sign Up</h2>
            <form
              className="auth-form"
              onSubmit={e => {
                e.preventDefault();
                createUser({ 
                  variables: { 
                    email: this.state.email,
                    familyName: this.state.familyName,
                    name: this.state.name,
                    password: this.state.password
                  } 
                });
              }}
            >
              <input
                placeholder="email address"
                value={this.state.email}
                onChange={(e) => this.handleInputChange("email", e.target.value)}
              ></input>
              <input
                placeholder="family name"
                value={this.state.familyName}
                onChange={(e) => this.handleInputChange("familyName", e.target.value)}
              ></input>
              <input
                placeholder="name"
                value={this.state.name}
                onChange={(e) => this.handleInputChange("name", e.target.value)}
              ></input>
              <input
                placeholder="password"
                value={this.state.password}
                type="password"
                onChange={(e) => this.handleInputChange("password", e.target.value)}
              ></input>
              <Link className="blue-link" to="/sign-in">Sign In</Link>
              {
                error && 
                <span className="error-msg">{error.message.replace('GraphQL error: ','')}</span>
              }
              <button type="submit" className="purple-element">Submit</button>
            </form>
          </div>
        )}
      </Mutation>
    );
  }
}

export default SignUp;
