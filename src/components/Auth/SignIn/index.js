import React, { PureComponent } from 'react';
import { Mutation } from 'react-apollo';
import { Link } from 'react-router-dom';

import { LOGIN_USER } from '../../../apollo/mutations';

import '../styles.scss';


class SignIn extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      email: "",
      password: "",
    }
  }

  handleInputChange(key, value) {
    this.setState({[key]: value})
  }

  setTokens(data) {
    localStorage.setItem("exreceipt_accessToken", data.accessToken);
    localStorage.setItem("exreceipt_idToken", data.idToken);
    localStorage.setItem("exreceipt_refreshToken", data.refreshToken);
    this.props.history.push('/');
  }

  render() {
    return (
      <Mutation mutation={LOGIN_USER} update={(cache, {data: {loginUser}}) => this.setTokens(loginUser)}>
        {(createUser, { error }) => (
          <div className="auth-container">
            <h2>Sign In</h2>
            <form
              className="auth-form"
              onSubmit={e => {
                e.preventDefault();
                createUser({ 
                  variables: { 
                    email: this.state.email,
                    password: this.state.password
                  } 
                });
              }}
            >
              <input
                placeholder="email address"
                value={this.state.email}
                onChange={(e) => this.handleInputChange("email", e.target.value)}
              ></input>
              <input
                placeholder="password"
                type="password"
                value={this.state.password}
                onChange={(e) => this.handleInputChange("password", e.target.value)}
              ></input>
              <Link className="blue-link" to="/sign-up">Sign Up</Link>
              {
                error && 
                <span className="error-msg">{error.message.replace('GraphQL error: ','')}</span>
              }
              <button type="submit" className="pink-element">Submit</button>
            </form>
          </div>
        )}
      </Mutation>
    );
  }
}

export default SignIn;
