import React from 'react';
import { shallow, mount } from 'enzyme';
import { ApolloProvider } from 'react-apollo';
import { BrowserRouter as Router } from 'react-router-dom';
import { spy } from 'sinon';

import SignIn from './index';

import client from '../../../apollo';
import { LOGIN_USER } from '../../../apollo/mutations';


it('Test handleInputChange method', () => {
  const wrapper = shallow(<SignIn />);

  const instance = wrapper.instance();

  instance.handleInputChange('email', 'user@example.com');

  expect(instance.state.email).toEqual('user@example.com');
});

it('Test mounted SignIn Component', () => {
  spy(client, 'mutate');

  const wrapper = mount(
    <ApolloProvider client={client}>
      <Router>
        <SignIn />
      </Router>
    </ApolloProvider>
  );

  const instance = wrapper.find(SignIn).instance()

  expect(wrapper.find('input')).toHaveLength(2);

  wrapper.find('input').first().simulate('change', { target: { value: 'user100@example.com' } });
  expect(instance.state.email).toEqual('user100@example.com');

  wrapper.find('input').at(1).simulate('change', { target: { value: 'password' } });
  expect(instance.state.password).toEqual('password');

  wrapper.find('button.pink-element').simulate('submit');
  expect(client.mutate.calledOnce).toEqual(true);

  expect(client.mutate.getCall(0).args[0].variables).toEqual({
    email: 'user100@example.com',
    password: 'password'
  });

  expect(client.mutate.getCall(0).args[0].mutation).toEqual(
    LOGIN_USER,
  );
});