import React, { PureComponent } from 'react';
import { Mutation } from 'react-apollo';
import { Link } from 'react-router-dom';

import { CONFIRM_USER } from '../../../apollo/mutations';

import '../styles.scss';


class Confirmation extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      confirmationCode: "",
      email: ""
    }
  }

  handleInputChange(key, value) {
    this.setState({[key]: value})
  }

  render() {
    return (
      <Mutation
        mutation={CONFIRM_USER}
        update={() => this.props.history.push("/sign-in")}
      >
        {(confirmUser, { data, error }) => (
          <div className="auth-container">
            <h2>Confirm an Account</h2>
            <p>We've sent you a confirmation code to you email address</p>
            <form
              className="auth-form"
              onSubmit={e => {
                e.preventDefault();
                confirmUser({ 
                  variables: {
                    confirmationCode: this.state.confirmationCode,
                    email: this.state.email,
                  } 
                });
              }}
            >
              <input
                placeholder="confirmation code"
                value={this.state.confirmationCode}
                onChange={(e) => this.handleInputChange("confirmationCode", e.target.value)}
              ></input>
              <input
                placeholder="email address"
                value={this.state.email}
                onChange={(e) => this.handleInputChange("email", e.target.value)}
              ></input>
              <Link className="blue-link" to="/sign-up">Sign Up</Link>
              {
                error && 
                <span className="error-msg">{error.message.replace('GraphQL error: ','')}</span>
              }
              <button type="submit" className="purple-element">Submit</button>
            </form>
          </div>
        )}
      </Mutation>
    );
  }
}

export default Confirmation;
