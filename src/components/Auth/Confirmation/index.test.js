import React from 'react';
import { shallow, mount } from 'enzyme';
import { ApolloProvider } from 'react-apollo';
import { BrowserRouter as Router } from 'react-router-dom';
import { spy } from 'sinon';

import Confirmation from './index';

import client from '../../../apollo';
import { CONFIRM_USER } from '../../../apollo/mutations';


it('Test handleInputChange method', () => {
  const wrapper = shallow(<Confirmation />);

  const instance = wrapper.instance();

  instance.handleInputChange('email', 'user@example.com');
  expect(instance.state.email).toEqual('user@example.com');

  instance.handleInputChange('confirmationCode', '00000');
  expect(instance.state.confirmationCode).toEqual('00000');
});

it('Test mounted Confirmation Component', () => {
  spy(client, 'mutate');

  const wrapper = mount(
    <ApolloProvider client={client}>
      <Router>
        <Confirmation />
      </Router>
    </ApolloProvider>
  );

  const instance = wrapper.find(Confirmation).instance()

  expect(wrapper.find('input')).toHaveLength(2);

  wrapper.find('input').first().simulate('change', { target: { value: '123456' } });
  expect(instance.state.confirmationCode).toEqual('123456');

  wrapper.find('input').at(1).simulate('change', { target: { value: 'user1@example.com' } });
  expect(instance.state.email).toEqual('user1@example.com');

  wrapper.find('button.purple-element').simulate('submit');
  expect(client.mutate.calledOnce).toEqual(true);

  expect(client.mutate.getCall(0).args[0].variables).toEqual({
    confirmationCode: '123456',
    email: 'user1@example.com'
  });

  expect(client.mutate.getCall(0).args[0].mutation).toEqual(
    CONFIRM_USER,
  );
});