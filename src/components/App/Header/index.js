import React, { PureComponent } from 'react';
import { Link, withRouter } from 'react-router-dom';

import './styles.scss';


class Header extends PureComponent {
  constructor(props) {
    super(props);

    this.signOut = this.signOut.bind(this);
  }

  signOut() {
    localStorage.removeItem("exreceipt_accessToken");
    localStorage.removeItem("exreceipt_idToken");
    localStorage.removeItem("exreceipt_refreshToken");
    this.props.history.replace("/sign-out");
  }

  render() {
    return (
      <div className="header">
        <h1>
          <Link to="/">Receipts</Link>
        </h1>
        {
          localStorage.getItem("exreceipt_accessToken") &&
          <button className="pink-element" onClick={this.signOut}>Sign Out</button>
        }
      </div>
    );
  }
}

export default withRouter(Header);
