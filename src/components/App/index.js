import React from 'react';
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';

import Header from './Header';

import SignUp from '../Auth/SignUp';
import SignIn from '../Auth/SignIn';
import Confirmation from '../Auth/Confirmation';

import Receipts from '../Receipts/List';
import ReceiptsForm from '../Receipts/Form';

import './styles.scss';


const App = () => (
  <Router>
    <div className="app">
      <section className="content">
        <Header/>
        <Switch>
          <Route path="/" exact component={Receipts} />
          <Route path="/receipt/add" component={ReceiptsForm} />
          <Route path="/sign-up" component={SignUp} />
          <Route path="/sign-in" component={SignIn} />
          <Route path="/confirmation" component={Confirmation} />
          <Redirect to='/' />
        </Switch>
      </section>
    </div>
  </Router>
)

export default App;
