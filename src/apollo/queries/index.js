import gql from 'graphql-tag';

const GET_USER_RECEIPTS = gql`
  {
    listCurrentUserReceipts {
      date
      description
      imageUrl
      itemName
      price
      uuid
    }
  }
`;

export {
  GET_USER_RECEIPTS
}