import gql from 'graphql-tag';

const LOGIN_USER = gql`
  mutation loginUser(
    $email: String!,
    $password: String!
  ) {
    loginUser(
      email: $email
      password: $password
    ) {
      accessToken
      idToken
      refreshToken
    }
  }
`;

const CREATE_USER = gql`
  mutation createUser(
    $email: String!,
    $familyName: String!,
    $name: String!,
    $password: String!
  ) {
    createUser(
      email: $email
      familyName: $familyName
      name: $name
      password: $password
    ) {
      uuid
    }
  }
`;

const CONFIRM_USER = gql`
  mutation confirmUserRegistration(
    $confirmationCode: String!,
    $email: String!,
  ) {
    confirmUserRegistration(
      confirmationCode: $confirmationCode
      email: $email
    )
  }
`;

const CREATE_RECEIPT = gql`
  mutation createReceipt(
    $date: String!,
    $description: String,
    $imageUrl: String!,
    $itemName: String!,
    $price: Int!,
  ) {
    createReceipt(
      date: $date,
      description: $description,
      imageUrl: $imageUrl,
      itemName: $itemName,
      price: $price,
    ) {
      uuid
    }
  }
`;

const REMOVE_RECEIPT = gql`
  mutation removeReceipt($uuid: UUID!) {
    removeReceipt(uuid: $uuid) {
      uuid
    }
  }
`;

export {
  LOGIN_USER,
  CREATE_USER,
  CONFIRM_USER,
  CREATE_RECEIPT,
  REMOVE_RECEIPT
}